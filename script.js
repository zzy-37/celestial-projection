const cnv = document.querySelector('canvas')
const ctx = cnv.getContext('2d')

const { PI, sin, cos, atan2, hypot, floor } = Math

ctx.fillStyle = 'red'
ctx.fillRect(0, 0, cnv.width, cnv.height)

const pixel_size = 1

const image_grid = 'celestial_grid_16k_print.jpg'
const image_milkyway = 'milkyway_2020_4k_print.jpg'

let va = 0
let vb = 0 //[-PI, PI]

const R = 200

getImageData(image_milkyway, imgdata => {
  function draw() {
    va += 0.02
    // vb += 0.03
    map_canvas_to_image(imgdata)
    requestAnimationFrame(draw)
  }

  console.log(imgdata.width, imgdata.height)
  draw()
})

function rotate(v, k, a) {
  const dot = k.x * v.x + k.y * v.y + k.z * v.z
  const vx = v.x * cos(a) + (k.y * v.z - k.z * v.y) * sin(a) + k.x * dot * (1 - cos(a))
  const vy = v.y * cos(a) + (k.z * v.x - k.x * v.z) * sin(a) + k.y * dot * (1 - cos(a))
  const vz = v.z * cos(a) + (k.x * v.y - k.y * v.x) * sin(a) + k.z * dot * (1 - cos(a))
  return { x: vx, y: vy, z: vz }
}

function map_canvas_to_image(imgdata) {
  for (let y = 0; y < cnv.height; ++y) {
    for (let x = 0; x < cnv.width; ++x) {
      const dx = x - cnv.width / 2
      const dy = y - cnv.height / 2

      const vec = rotate(
        {
          x: dx * cos(va) - dy * sin(va),
          y: dx * sin(va) + dy * cos(va),
          z: R
        },
        {
          x: cos(va),
          y: sin(va),
          z: 0
        },
        vb)

      const u = atan2(vec.y, vec.x)
      const r = hypot(vec.x, vec.y)
      const v = atan2(r, vec.z)

      const cx = floor(lerp(0, imgdata.width, u / (2 * PI)))
      const cy = floor(lerp(0, imgdata.height, v / PI))

      const i = get_pixel_index(cx, cy, imgdata.width)
      // ctx.fillStyle = 'green'
      ctx.fillStyle = `rgb(
        ${imgdata.data[i]},
        ${imgdata.data[i + 1]},
        ${imgdata.data[i + 2]})`
      ctx.fillRect(x, y, pixel_size, pixel_size)
    }
  }
}

function map_image_to_canvas(imgdata) {
  for (let x = 0; x < imgdata.width; ++x) {
    const radx = lerp(0, PI * 2, x / imgdata.width)
    for (let y = 0; y < imgdata.height; ++y) {
      const rady = lerp(0, PI, y / imgdata.height)

      const r = sin(rady) * 300
      const cx = r * cos(radx) + cnv.width / 2
      const cy = r * sin(radx) + cnv.height / 2

      if (cx >= 0 && cx < cnv.width && cy >= 0 && cy < cnv.height) {
        const j = get_pixel_index(x, y, imgdata.width)

        ctx.fillStyle = `rgb(
          ${imgdata.data[j]},
          ${imgdata.data[j + 1]},
          ${imgdata.data[j + 2]})`
        ctx.fillRect(cx, cy, pixel_size, pixel_size)
      }
    }
  }
}

function fillCircle(ctx, x, y, r) {
  ctx.beginPath()
  ctx.arc(x, y, r, 0, 2 * PI)
  ctx.fill()
}

function getImageData(filePath, callback) {
  const img = new Image()
  img.src = filePath
  img.onload = () => {
    const cnv1 = document.createElement('canvas')
    cnv1.width = img.naturalWidth
    cnv1.height = img.naturalHeight
    const ctx = cnv1.getContext('2d')
    ctx.drawImage(img, 0, 0)
    callback(ctx.getImageData(0, 0, cnv1.width, cnv1.height))
    cnv.after(img)
  }
}

function lerp(a, b, t) {
  return a + (b - a) * t
}

function get_pixel_index(x, y, stride) {
  return (x + y * stride) * 4
}